DEAD_PROXY = {
    mode: "fixed_servers",
    rules: {
        singleProxy: {
            host: "127.0.0.1",
            port: 1111
        }
    }
};

updateUI();

chrome.browserAction.onClicked.addListener(function(tab) {
    chrome.proxy.settings.get({}, function(config) {
        if (isDeadProxy(config)) {
            online(updateUI);
        } else {
            offline(updateUI);
        }
    });
});


function offline(callback) {
	chrome.proxy.settings.set({value: DEAD_PROXY, scope: 'regular'}, callback);
}

function online(callback) {
	chrome.proxy.settings.clear({scope: 'regular'}, callback);
}

function isDeadProxy(config) {
    return config.levelOfControl == 'controlled_by_this_extension'
            && config.value.mode == DEAD_PROXY.mode
            && config.value.rules.singleProxy
            && config.value.rules.singleProxy.host == DEAD_PROXY.rules.singleProxy.host
            && config.value.rules.singleProxy.port == DEAD_PROXY.rules.singleProxy.port;
}

function updateUI () {
    chrome.proxy.settings.get({}, function(config) {
        console.log(JSON.stringify(config));
        if (isDeadProxy(config)) {
            chrome.browserAction.setTitle({title :"Chrome is offline now. Click to go online."});
            chrome.browserAction.setIcon({path: {"19": "off-19.png", "38": "off-38.png"}});
        } else {
            chrome.browserAction.setTitle({title :"Chrome is online now. Click to go offline."});
            chrome.browserAction.setIcon({path: {"19": "on-19.png", "38": "on-38.png"}});
        }
    });
}